/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frozen.neo4j.user;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 *
 * @author Shado
 */
@NodeEntity
public class DummyNode {

    @Id
    @GeneratedValue
    private Long id;

    public DummyNode() {
    }

    public Long getId() {
        return id;
    }
}
