/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frozen.neo4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.ogm.config.ClasspathConfigurationSource;
import org.neo4j.ogm.config.Configuration;
import org.neo4j.ogm.session.SessionFactory;

/**
 *
 * @author Shado
 */
public class Neo4J {
    
    private final static Logger LOGGER = LogManager.getLogger(Neo4J.class);

    public final static SessionFactory SESSION_FACTORY;

    static {
        ClasspathConfigurationSource configurationSource = null;
        try {
            configurationSource
                    = new ClasspathConfigurationSource("ogm.properties");
        } catch (NullPointerException ex) {
            LOGGER.warn("ogm.properties not found, defaults to null session!");
        } // try
        if (configurationSource != null) {
            Configuration configuration
                    = new Configuration.Builder(configurationSource).build();
            SESSION_FACTORY = new SessionFactory(configuration,
                    "com.naturo.n4j.model");
        } else {
            SESSION_FACTORY = null;
        } // try
    }
}
