package com.frozen.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Shado
 */
public class Commons {
    
    private final static Set<String> CORS_WHITE_LIST = new HashSet<>(Arrays.asList(
            "https://dev.frozen.tech/",
            "https://www.dev.frozen.tech/",
            "http://dev.frozen.tech/",
            "http://www.dev.frozen.tech/"));

    private final static Logger LOGGER
            = LogManager.getLogger(Commons.class);

    public final static ObjectMapper JSON_MAPPER = new ObjectMapper();

    /**
     * Check if origin is setted
     *
     * @param origin String
     * @return a String if origin is setted, null if not
     */
    public static String check(String origin) {
        if (CORS_WHITE_LIST.contains(origin)) {
            return origin;
        } // if
        return null;
    }

    /**
     * Check if all parameters are setted
     *
     * @param params String
     * @return a boolean, true if yes, false if no
     */
    public static boolean allSetted(String... params) {
        return Arrays.asList(params).stream().
                allMatch(Commons::setted);
    }

    /**
     * Check if at least one parameter is setted
     *
     * @param params String
     * @return a boolean, true if yes, false if no
     */
    public static boolean anySetted(String... params) {
        return Arrays.asList(params).stream().
                anyMatch(Commons::setted);
    }

    /**
     * Check if a parameter is setted
     *
     * @param param String
     * @return a boolean, true if yes, false if no
     */
    public static boolean setted(String param) {
        return param != null && !param.isEmpty();
    }
}
