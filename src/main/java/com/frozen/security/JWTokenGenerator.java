/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frozen.security;

import io.jsonwebtoken.Jwts;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.TemporalAmount;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

/**
 *
 * @author Shado
 */
public class JWTokenGenerator {
    
    private final static TemporalAmount HEADER_TOKEN_TIME_TO_LIVE = Duration.ofHours(24);
    private final static TemporalAmount BODY_TOKEN_TIME_TO_LIVE = Duration.ofSeconds(1);
    private final static String PRIVATE_PATH = "jwt.private.crt";
    public final static String PUBLIC_PATH = "jwt.public.crt";

    private static PrivateKey PRIVATE_KEY;

    /**
     * Generates a jwt for user
     *
     * @param email String
     * @param firstName String
     * @param profile String
     * @return a user's jwt
     */
    public final static String generateHeaderToken(String email, String firstName, String profile) {
        TemporalAmount timeToLive = HEADER_TOKEN_TIME_TO_LIVE;
        if (PRIVATE_KEY == null) {
            init();
        } // if

        Instant now = Instant.now();
        return Jwts.builder().
                setSubject("userToken"). // sub
                setId(UUID.randomUUID().toString()). // jti
                setIssuedAt(Date.from(now)). // iat
                setExpiration(Date.from(now.plus(timeToLive))). // exp
                
                signWith(PRIVATE_KEY).
                compact();
    }

    /**
     * Generates a jwt response for json sent by endpoints
     *
     * @param response Object
     * @return a response's jwt
     */
    public final static String generateBodyToken(Object response) {
        if (PRIVATE_KEY == null) {
            init();
        } // if
        Instant now = Instant.now();
        return Jwts.builder().
                setSubject("userToken"). // sub
                setId(UUID.randomUUID().toString()). // jti
                setIssuedAt(Date.from(now)). // iat
                setExpiration(Date.from(now.plus(BODY_TOKEN_TIME_TO_LIVE))). // exp
                claim("response", response).
                signWith(PRIVATE_KEY).
                compact();
    }

    private static void init() {
        try {
            StringBuilder builder = new StringBuilder();
            BufferedReader in = new BufferedReader(new FileReader(
                    PRIVATE_PATH));
            while (in.ready()) {
                String line = in.readLine();
                if (!line.startsWith("-----")) {
                    builder.append(line);
                } // if
            } // while
            PRIVATE_KEY = KeyFactory.getInstance("RSA").
                    generatePrivate(new PKCS8EncodedKeySpec(Base64.getDecoder().
                            decode(builder.toString())));
        } catch (IOException
                | NoSuchAlgorithmException
                | InvalidKeySpecException ex) {
            throw new RuntimeException(ex);
        } // try
    }
}
