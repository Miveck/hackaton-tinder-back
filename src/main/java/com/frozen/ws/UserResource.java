/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frozen.ws;

import com.frozen.neo4j.user.DummyNode;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

/**
 *
 * @author Shado
 */
@Path("/user")
public class UserResource {

    @Path("dummyEndpoint")
    public static Response DummyEndpoint() {
        DummyNode test = new DummyNode();
        return Response.ok("Dummy endpoint id : " + test.getId()).build();
    }
}
