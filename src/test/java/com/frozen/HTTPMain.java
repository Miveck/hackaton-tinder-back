/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frozen;

import com.frozen.controller.Commons;
import jakarta.ws.rs.container.ContainerRequestContext;
import jakarta.ws.rs.container.ContainerResponseContext;
import jakarta.ws.rs.container.ContainerResponseFilter;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.Provider;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.monitoring.ApplicationEvent;
import org.glassfish.jersey.server.monitoring.ApplicationEventListener;
import org.glassfish.jersey.server.monitoring.RequestEvent;
import org.glassfish.jersey.server.monitoring.RequestEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lemad_coder
 */
public class HTTPMain {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(HTTPMain.class);

    private final static String ADDRESS = "0.0.0.0";
    private final static int PORT = 8080;
    
    /**
     * @param args the command line arguments
     * @throws java.io.IOException if an I/O exception of some sort has occured
     */
    public static void main(String... args) throws IOException {
        init();

        final ResourceConfig config = new ResourceConfig().
                packages("com.naturo.ws").
                register(CorsFilter.class).
                register(ExceptionListener.class);
        GrizzlyHttpServerFactory.createHttpServer(
                URI.create("http://" + ADDRESS + ':' + PORT),
                config);
    }
    
    private static void init() {
        java.util.logging.LogManager.getLogManager().reset();
        org.slf4j.bridge.SLF4JBridgeHandler.install();
    }
    
    @Provider
    final static class RequestDataLogger implements ContainerResponseFilter {

        private final static Logger LOGGER = LoggerFactory.getLogger(RequestDataLogger.class);

        @Override
        public void filter(ContainerRequestContext requestContext,
                ContainerResponseContext responseContext) throws IOException {
            LOGGER.debug("Headers:");
            for (Map.Entry<String, List<String>> header
                    : requestContext.getHeaders().entrySet()) {
                LOGGER.debug(header.getKey());
                for (String value : header.getValue()) {
                    LOGGER.debug("\t" + value);
                } // for
            } // for
            LOGGER.debug("Query parameters:");
            for (Map.Entry<String, List<String>> queryParameter
                    : requestContext.getUriInfo().getQueryParameters().
                            entrySet()) {
                LOGGER.debug(queryParameter.getKey());
                for (String value : queryParameter.getValue()) {
                    LOGGER.debug("\t" + value);
                } // for
            } // for
            LOGGER.debug("Path parameters:");
            for (Map.Entry<String, List<String>> pathParameter : requestContext.
                    getUriInfo().getPathParameters().entrySet()) {
                LOGGER.debug(pathParameter.getKey());
                for (String value : pathParameter.getValue()) {
                    LOGGER.debug("\t" + value);
                } // for
            } // for
        }

    }
    
    private static void addCorsHeaders(ContainerRequestContext requestContext,
            ContainerResponseContext responseContext) throws IOException {
        LOGGER.debug("Adding CORS headers for request " + requestContext.getMethod()+" @ " + requestContext.getUriInfo().getPath());
        MultivaluedMap<String, Object> headers = responseContext.
                getHeaders();
        String origin;
        if (requestContext.getUriInfo().getPath().startsWith("public")) {
            origin = "*";
        } else {
            origin = Commons.check(requestContext.getHeaderString("Origin"));
        } // if
        if (origin != null) {
            headers.add("Access-Control-Allow-Origin", origin);
        } // if
        headers.add("Access-Control-Allow-Headers",
                "CSRF-Token, X-Requested-By, Authorization, Content-Type");
        headers.add("Access-Control-Allow-Credentials", "true");
        headers.add("Access-Control-Allow-Methods",
                "GET, POST, PUT, DELETE, OPTIONS, HEAD");
    }
    
    @Provider
    private final static class CorsFilter implements ContainerResponseFilter {

        @Override
        public void filter(ContainerRequestContext requestContext,
                ContainerResponseContext responseContext) throws IOException {
            addCorsHeaders(requestContext, responseContext);
        }
    }

    @Provider
    final static class ExceptionListener implements ApplicationEventListener, RequestEventListener {

        private final static Logger LOGGER = LoggerFactory.getLogger(ExceptionListener.class);

        @Override
        public void onEvent(ApplicationEvent event) {
        }

        @Override
        public RequestEventListener onRequest(RequestEvent requestEvent) {
            return this;
        }

        @Override
        public void onEvent(RequestEvent event) {
            switch (event.getType()) {
                case ON_EXCEPTION:
                    StringBuilder builder = new StringBuilder("Exception");
                    for (StackTraceElement element : event.getException().getStackTrace()) {
                        builder.append("\n\t").append(element.toString());
                    } // for
                    LOGGER.error(builder.toString(), event.getException().getCause());
                    break;
                default:
            } // switch
        }
    }
    
}
