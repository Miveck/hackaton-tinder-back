/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frozen;

import com.frozen.neo4j.Neo4J;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.neo4j.ogm.session.SessionFactory;

/**
 *
 * @author Bobi
 */
public class Neo4jHelpers {

    public static final String PACKAGE = "com.naturo.n4j.model";

    public static void replaceNeo4JSessionFactory(SessionFactory sessionFactory)
            throws ReflectiveOperationException {

        // get Neo4J.SESSION field
        Field sessionFactoryField = Neo4J.class.getDeclaredField("SESSION_FACTORY");

        // set it accessible
        sessionFactoryField.setAccessible(true);
        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(sessionFactoryField,
                sessionFactoryField.getModifiers() & ~Modifier.FINAL);

        // modify it
        sessionFactoryField.set(null, sessionFactory);

        // set it back to default accessibility
        modifiersField.setInt(sessionFactoryField,
                sessionFactoryField.getModifiers() & Modifier.FINAL);
        modifiersField.setAccessible(false);
        sessionFactoryField.setAccessible(false);
    }
}
