package com.frozen.ws;

import static com.frozen.Neo4jHelpers.PACKAGE;
import static com.frozen.Neo4jHelpers.replaceNeo4JSessionFactory;
import static com.frozen.neo4j.Neo4J.SESSION_FACTORY;
import jakarta.ws.rs.core.Response;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.neo4j.ogm.config.Configuration;
import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.neo4j.ogm.config.Configuration;
import org.neo4j.ogm.cypher.Filter;
import org.neo4j.ogm.session.SessionFactory;
import org.testng.annotations.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Shado
 */
public class UserResourceTest {

    private final static Logger LOGGER
            = LogManager.getLogger(UserResourceTest.class);
    private final static File DB_FILE = new File(UserResource.class.getSimpleName() + ".db");

    @BeforeClass
    public static void setUpNeo4j() throws InterruptedException, IOException,
            ReflectiveOperationException, NoSuchFieldException,
            IllegalAccessException, NoSuchAlgorithmException {
        try {
            FileUtils.forceDelete(DB_FILE);
        } catch (FileNotFoundException ex) {
            // continue
        } // try

        Configuration configuration = new Configuration.Builder().
                uri(DB_FILE.toURI().toString()).build();
        SessionFactory sessionFactory
                = new SessionFactory(configuration, PACKAGE);
        replaceNeo4JSessionFactory(sessionFactory);

        Session session = SESSION_FACTORY.openSession();
    }

    @AfterClass
    public static void tearDownNeo4j() throws IOException {
        SESSION_FACTORY.close();
        FileUtils.forceDelete(DB_FILE);
    }

    @Test
    public static void testDummyNode() {
        Response response = UserResource.DummyEndpoint();
        LOGGER.debug("response : " + response.getEntity());
    }
}
